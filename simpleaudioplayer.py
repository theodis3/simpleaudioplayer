import os, _thread, time, sys, html
import gi

gi.require_version("Gtk", "3.0")
gi.require_version("Gst", "1.0")
from gi.repository import Gst, GObject, Gtk

from os import listdir
from os.path import isfile, join, expanduser

musicpath = join(expanduser("~"), "Music")
musicfiles = [f for f in listdir(musicpath) if isfile(join(musicpath, f))]

class ListBoxRowWithData(Gtk.ListBoxRow):
    def __init__(self, data):
        super(Gtk.ListBoxRow, self).__init__()
        self.file = data
        self.path = join(musicpath, self.file)
        self.label = Gtk.Label(label=self.file)
        self.label.set_halign(Gtk.Align.START)
        self.add(self.label)

class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.duration = 0
        self.paused = False
        self.playing_row = None

        # Column
        self.column = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        self.header = Gtk.HeaderBar()
        self.header.set_show_close_button(True)
        self.set_titlebar(self.header)

        # Button row
        # self.button_row = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        # self.column.pack_start(self.button_row, False, False, 0)

        # Buttons
        self.buttons = {}
        self.buttons["previous"] = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_MEDIA_PREVIOUS))
        self.buttons["previous"].connect("clicked", self.on_previous_clicked)
        self.buttons["play"] = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY))
        self.buttons["play"].connect("clicked", self.on_play_clicked)
        self.buttons["stop"] = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_MEDIA_STOP))
        self.buttons["stop"].connect("clicked", self.on_stop_clicked)
        self.buttons["next"] = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_MEDIA_NEXT))
        self.buttons["next"].connect("clicked", self.on_next_clicked)

        for key in self.buttons:
            #self.button_row.pack_start(self.buttons[key], True, False, 0)
            self.header.pack_start(self.buttons[key])

        # Progress
        self.progress = Gtk.ProgressBar()
        self.column.pack_start(self.progress, False, False, 0)

        # Files
        self.files_window = Gtk.ScrolledWindow()
        self.files = Gtk.ListBox()
        self.files.set_selection_mode(Gtk.SelectionMode.SINGLE)
        for mfile in musicfiles:
            self.files.add(ListBoxRowWithData(mfile))
        self.files.connect("row-activated", self.on_row_activated)
        self.files_window.add(self.files)
        self.column.pack_start(self.files_window, True, True, 0)

        # Player
        self.player = Gst.ElementFactory.make("playbin", "player")
        fakesink = Gst.ElementFactory.make("fakesink", "fakesink")
        self.player.set_property("video-sink", fakesink)
        bus = self.player.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message)

        self.add(self.column)

    def play(self, row):
        selected_row = self.files.get_selected_row()
        if self.paused and row == self.playing_row:
            self.resume()
        elif row is not None:
            self.stop()
            self.playing_row = row
            self.header.props.title = self.playing_row.file
            print("Playing " + self.playing_row.file +  "...")
            self.playing_row.label.set_markup("<b>" + html.escape(self.playing_row.file) + "</b>")
            filepath = self.playing_row.path
            self.player.set_property("uri", "file://" + filepath)
            self.player.set_state(Gst.State.PLAYING)
            self.play_thread_id = _thread.start_new_thread(self.play_thread, ())
            if(row == selected_row):
                self.buttons["play"].set_image(Gtk.Image(stock=Gtk.STOCK_MEDIA_PAUSE))
    def resume(self):
        self.player.set_state(Gst.State.PLAYING)
        self.paused = False
        self.buttons["play"].set_image(Gtk.Image(stock=Gtk.STOCK_MEDIA_PAUSE))

    def pause(self):
        self.player.set_state(Gst.State.PAUSED)
        self.paused = True
        self.buttons["play"].set_image(Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY))

    def playpause(self):
        if(self.playing_row is not None):
            if(self.paused):
                self.resume()
            else:
                self.pause()

    def stop(self):
        self.play_thread_id = None
        if(self.duration > 0):
            print("Stopping...")
            self.playing_row.label.set_markup(html.escape(self.playing_row.file))

            # Terrible, terrible hacks make me sad :(
            self.player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.TRICKMODE, self.duration)
            last = None
            pos_int = self.player.query_position(Gst.Format.TIME)
            while pos_int.cur < self.duration and pos_int.cur != last:
                time.sleep(0.1)
                last = pos_int.cur
                pos_int = self.player.query_position(Gst.Format.TIME)
            time.sleep(0.5)
            self.duration = 0
            self.progress.set_fraction (0)
            self.player.set_state(Gst.State.NULL)
            self.playing_row = None
            self.buttons["play"].set_image(Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY))

    def next(self):
        cur_index = self.playing_row.get_index()
        self.stop()
        self.play(self.files.get_row_at_index(cur_index + 1))

    def previous(self):
        cur_index = self.playing_row.get_index()
        self.stop()
        self.play(self.files.get_row_at_index(cur_index - 1))

    def done(self):
        print("Finished song")
        self.next()

    def play_thread(self):
        play_thread_id = self.play_thread_id
        time.sleep(0.1)

        # Get the duration of the song
        while play_thread_id == self.play_thread_id:
            time.sleep(0.05)
            dur_int = self.player.query_duration(Gst.Format.TIME)
            if dur_int[0] == True:
                self.duration = dur_int.duration
                break

        # Update the progress
        while play_thread_id == self.play_thread_id:
            pos_int = self.player.query_position(Gst.Format.TIME)
            if(pos_int[0] == True):
                fraction = pos_int.cur / self.duration
                self.progress.set_fraction (fraction)
                if(pos_int.cur + 200000000 >= self.duration):
                    self.done()
            time.sleep(0.2)
    def on_previous_clicked(self, widget):
        self.previous()
    def on_next_clicked(self, widget):
        self.next()
    def on_play_clicked(self, widget):
        row = self.files.get_selected_row()
        if(row == self.playing_row and not self.paused):
            self.pause()
        else:
            self.play(row)
    def on_stop_clicked(self, widget):
        self.stop()
    def on_row_activated(self, listbox, row):
        row = self.files.get_selected_row()
        if(row == self.playing_row and not self.paused):
            self.buttons["play"].set_image(Gtk.Image(stock=Gtk.STOCK_MEDIA_PAUSE))
        else:
            self.buttons["play"].set_image(Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY))
    def on_message(self, bus, message):
        pass

Gst.init(None)
win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
